
import java.util.Scanner;


//Найти и напечатать, сколько раз повторяется в тексте каждое слово.
//and, also - also. and - also! check
public class Lab9 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println( "Введите строку:");
        String str = sc1.nextLine();
        str = str.toLowerCase();
        //Строка для проверки and, also - also. and - also! check
        
        //String[] s = str.split("(?U)[\t\n\r\\.,\\:\\;\\/\\-\\|\\*\\+&^%$#@!`{}[]()\\s]+");  //не работает. учи Регулярные выражения!!!
        String[] s = str.split("[^a-zA-z]\\s+"); //неплохая конструкция для разбивки строки на слова, убирая лишние пробелы. Однако не реагирует на открывающиеся кавычки, например
                
        int[] n = new int[s.length]; //массив для запоминания кол-ва вхождения слова в текст.
        
        for ( int i = 0; i < s.length; i++) {
            s[i] = s[i].trim(); //обрубаем первые и последние пробелы
        }
        
        for ( int i = 0; i < s.length; i++) {
            for (int j = 1; j < s.length; j++){
                if ((!s[i].equals("0")) && (s[i].equals(s[j])) && (i != j)) { //если слово не равно 0   И   слова совпадают    И     и это не одно и то же слово
                    s[j] = "0";    //заменяем совпавшее слово на 0, оно на нам больше не нужно
                    n[i]++;        //увеличиваем элемент массива
                }
            }    
        }
        
        for ( int i = 0; i < s.length; i++) {
            if (!s[i].equals("0")) System.out.println("Слово " + s[i] + " встречается " + (n[i]+1) + " раз(а)"); //выводим n[i]+1 потому что нужно учесть и самое первое вхождение слова в текст
        }
        
    }    
       
}

